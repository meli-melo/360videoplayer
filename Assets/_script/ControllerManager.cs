﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerManager : MonoBehaviour
{
    public delegate void onMoveLeft();
    public onMoveLeft moveLeft;

    public delegate void onMoveRight();
    public onMoveRight moveRight;

    public delegate void onSelectDown();
    public onSelectDown selectDown;

    [SerializeField]
    private bool _controllerDetected = false;

    private float _horizontal = 0f;
    private bool _held = false;

    // Start is called before the first frame update
    void Start()
    {
        string[] names = Input.GetJoystickNames();
        if (names.Length > 0)
            _controllerDetected = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_controllerDetected)
        {
            _horizontal = Input.GetAxis("Horizontal");

            if (Input.GetButtonDown("Fire1"))
                selectDown?.Invoke();

            if (!_held)
            {
                if (_horizontal > 0.5f)
                {
                    moveRight?.Invoke();
                    _held = true;
                }        
                if (_horizontal < -0.5f)
                {
                    moveLeft?.Invoke();
                    _held = true;
                }
            }
            else
            {
                if (_horizontal == 0f)
                    _held = false;
            }
        }
    }
}
