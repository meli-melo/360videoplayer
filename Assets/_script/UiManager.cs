﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _headObject;
    [SerializeField]
    private ControllerManager _controllerManager;
    [SerializeField]
    private Image _cursor;

    private RectTransform _rectTransform;

    public delegate void onVolumeDown();
    public onVolumeDown volumeDown;

    public delegate void onVolumeUp();
    public onVolumeUp volumeUp;

    public delegate void onPlay();
    public onPlay play;

    public delegate void onPause();
    public onPause pause;

    public delegate void onPrevious();
    public onPrevious previous;

    public delegate void onStop();
    public onStop stop;

    public delegate void onNext();
    public onNext next;

    // Start is called before the first frame update
    void Start()
    {
        if(_controllerManager != null)
        {
            _controllerManager.moveLeft += MoveCursorLeft;
            _controllerManager.moveRight += MoveCursorRight;
            _controllerManager.selectDown += SelectButton;
        }
        _rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        if(_headObject != null)
        {
            _rectTransform.rotation = Quaternion.Euler(0f, _headObject.transform.eulerAngles.y, 0f);
        }
    }

    private void MoveCursorLeft()
    {
        if(_cursor.rectTransform.localPosition.x > -198)
        {
            _cursor.rectTransform.localPosition = new Vector3(_cursor.rectTransform.localPosition.x - 66.5f, 0f, 0f);
        }
    }

    private void MoveCursorRight()
    {
        if (_cursor.rectTransform.localPosition.x < 197)
        {
            _cursor.rectTransform.localPosition = new Vector3(_cursor.rectTransform.localPosition.x + 66.5f, 0f, 0f);
        }
    }

    private void SelectButton()
    {
        float cursorPosition = _cursor.rectTransform.localPosition.x;

        if (cursorPosition == -200f)
            volumeDown?.Invoke();
        if (cursorPosition == -133.5f)
            volumeUp?.Invoke();
        if (cursorPosition == -67f)
            play?.Invoke();
        if (cursorPosition == -0.5f)
            pause?.Invoke();
        if (cursorPosition == 66f)
            previous?.Invoke();
        if (cursorPosition == 132.5f)
            stop?.Invoke();
        if (cursorPosition == 199f)
            next?.Invoke();
    }

    private void OnDestroy()
    {
        if (_controllerManager != null)
        {
            _controllerManager.moveLeft -= MoveCursorLeft;
            _controllerManager.moveRight -= MoveCursorRight;
            _controllerManager.selectDown -= SelectButton;
        }
    }
}
