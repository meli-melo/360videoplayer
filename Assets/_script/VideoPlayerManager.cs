﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoPlayerManager : MonoBehaviour
{
    [SerializeField]
    private VideoPlayer _videoPlayer;
    [SerializeField]
    private UiManager _uiManager;
    // Start is called before the first frame update
    void Start()
    {
        if(_uiManager != null)
        {
            _uiManager.volumeDown += VolumeDown;
            _uiManager.volumeUp += VolumeUp;
            _uiManager.play += Play;
            _uiManager.pause += Pause;
            _uiManager.previous += Previous;
            _uiManager.stop += Stop;
            _uiManager.next += Next;
        }
    }

    private void VolumeDown()
    {
        _videoPlayer.SetDirectAudioVolume(0, _videoPlayer.GetDirectAudioVolume(0) - 0.1f);
        if (_videoPlayer.GetDirectAudioVolume(0) < 0f)
            _videoPlayer.SetDirectAudioVolume(0, 0f);
    }

    private void VolumeUp()
    {
        _videoPlayer.SetDirectAudioVolume(0, _videoPlayer.GetDirectAudioVolume(0) + 0.1f);
        if (_videoPlayer.GetDirectAudioVolume(0) > 1f)
            _videoPlayer.SetDirectAudioVolume(0, 1f);
    }

    private void Play()
    {
        _videoPlayer.Play();
    }

    private void Pause()
    {
        _videoPlayer.Pause();
    }

    private void Previous()
    {

    }

    private void Stop()
    {
        _videoPlayer.Stop();
    }

    private void Next()
    {

    }

    private void OnDestroy()
    {
        if (_uiManager != null)
        {
            _uiManager.volumeDown -= VolumeDown;
            _uiManager.volumeUp -= VolumeUp;
            _uiManager.play -= Play;
            _uiManager.pause -= Pause;
            _uiManager.previous -= Previous;
            _uiManager.stop -= Stop;
            _uiManager.next -= Next;
        }
    }
}
